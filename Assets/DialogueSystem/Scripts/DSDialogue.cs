using System;
using DialogueSystem.ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DialogueSystem
{
    public class DSDialogue : MonoBehaviour
    {
        //Dialogue Scriptable Objects
        [SerializeField] private DSDialogueContainerSO dialogueContainer;
        [SerializeField] private DSDialogueGroupSO dialogueGroup;
        [SerializeField] private DSDialogueSO dialogue;
        
        // Filters
        [SerializeField] private bool groupedDialogues;
        [SerializeField] private bool startingDialoguesOnly;
        
        // Indexes
        [SerializeField] private int selectedDialogueGroupIndex = 0;
        [SerializeField] private int selectedDialogueIndex = 0;
        
        //Test
        [SerializeField] private TextMeshProUGUI text = null;
        [SerializeField] private Button choice1 = null;
        [SerializeField] private Button choice2 = null;
        
        [SerializeField] private DSDialogueSO currentDialogue;

        private void Start()
        {
            currentDialogue = dialogue;
            SetButtonText();
            
            if (text)
            {
                text.text = currentDialogue.Text;
            }

            if (choice1 && choice2)
            {
                choice1.onClick.AddListener(() =>
                {
                    SetNextDialogue(0);
                    SetButtonText();
                });
                choice2.onClick.AddListener(() =>
                {
                    SetNextDialogue(1);
                    SetButtonText();
                });
            }
        }

        private void SetButtonText()
        {
            int choicesCount = currentDialogue.Choices.Count;
            bool nextDialogue1IsNull = true;
            bool nextDialogue2IsNull = true;
            
            if(choicesCount >= 1)
                nextDialogue1IsNull = currentDialogue.Choices[0].NextDialogue == null;
            if(choicesCount == 2)
                nextDialogue2IsNull = currentDialogue.Choices[1].NextDialogue == null;
            
            Text choice1Text = choice1.GetComponentInChildren<Text>();
            Text choice2Text = choice2.GetComponentInChildren<Text>();

            if (choicesCount <= 0 || nextDialogue1IsNull && nextDialogue2IsNull)
            {
                choice1Text.text = "";
                choice2Text.text = "";
                
            }
            else if (!nextDialogue1IsNull && !nextDialogue2IsNull)
            {
                choice1Text.text = currentDialogue.Choices[0].NextDialogue.DialogueName;
                choice2Text.text = currentDialogue.Choices[1].NextDialogue.DialogueName;
            }
            else
            {
                choice1Text.text = "Next";
                choice2Text.text = "";
            }
        }

        private void SetNextDialogue(int index)
        {
            if (index > currentDialogue.Choices.Count - 1 || currentDialogue.Choices[index] == null || currentDialogue.Choices[index].NextDialogue == null)
                return;

            currentDialogue = currentDialogue.Choices[index].NextDialogue;
            text.text = currentDialogue.Text;
        }
    }
}