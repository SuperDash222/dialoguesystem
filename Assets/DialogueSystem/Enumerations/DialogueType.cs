namespace DialogueSystem.Enumerations
{ 
    public enum DialogueType
    {
        SingleChoice,
        MultipleChoice,
    }
}