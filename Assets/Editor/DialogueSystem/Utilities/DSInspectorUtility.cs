using System;
using UnityEditor;


namespace DialogueSystem.Utilities
{ 
    public static class DSInspectorUtility
    {
        public static void DrawDisabledFields(Action action)
        {
            EditorGUI.BeginDisabledGroup(true);
            
            action.Invoke();
            
            EditorGUI.EndDisabledGroup();
        }

        public static void DrawHeader(string label)
        {
            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
        }

        public static void DrawPropertyField(this SerializedProperty serializedProperty)
        {
            EditorGUILayout.PropertyField(serializedProperty);
        }

        public static int DrawPopup(string label, SerializedProperty selectedIndexProperty, string [] options)
        {
            return EditorGUILayout.Popup(label, selectedIndexProperty.intValue, options);
        }
        
        public static int DrawPopup(string label, int selectedIndex, string [] options)
        {
            return EditorGUILayout.Popup(label, selectedIndex, options);
        }
    }
}