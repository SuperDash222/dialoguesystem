using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;


namespace DialogueSystem.Windows
{
    using Utilities;
    using Elements;
    using Enumerations;
    using Data.Error;
    using Data.Save;
    
    public class DSGraphView : GraphView
    {
        private DSSearchWindow searchWindow;
        private DSEditorWindow editorWindow;
        private MiniMap miniMap;

        private SerializableDictionary<string, DSNodeErrorData> ungroupedNodes;
        private SerializableDictionary<string, DSGroupErrorData> groups;
        private SerializableDictionary<DSGroup, SerializableDictionary<string, DSNodeErrorData>> groupedNodes;

        private int nameErrorsAmount;

        public int RepeatedNamesAmount
        {
            get => nameErrorsAmount;
            set
            {
                nameErrorsAmount = value;
                if (nameErrorsAmount == 0)
                {
                    editorWindow.EnableSaving();
                }
                else
                {
                    editorWindow.DisableSaving();
                }
            }
        }

        public DSGraphView(DSEditorWindow dsEditorWindow)
        {
            editorWindow = dsEditorWindow;

            ungroupedNodes = new SerializableDictionary<string, DSNodeErrorData>();
            groups = new SerializableDictionary<string, DSGroupErrorData>();
            groupedNodes = new SerializableDictionary<DSGroup, SerializableDictionary<string, DSNodeErrorData>>();
            
            AddManipulators();
            AddSearchWindow();
            AddMiniMap();

            AddGridBackground();
            AddStyles();
            AddMiniMapStyles();
            
            OnElementsDeleted();
            OnGroupElementsAdded();
            OnGroupElementsRemoved();
            OnGroupRenamed();
            OnGraphViewChanged();
        }

        #region Elements Addition
        private void AddGridBackground()
        {
            GridBackground gridBackground = new GridBackground();
            VisualElementExtensions.StretchToParentSize(gridBackground);
            Insert(0, gridBackground);
        }

        private void AddStyles()
        {
            this.AddStyleSheets(
                "DialogueSystem/DSGraphViewStyles.uss",
                "DialogueSystem/DSNodeStyles.uss"
                );
        }
        
        private void AddMiniMapStyles()
        {
            StyleColor backgroundColor = new StyleColor(new Color32(29, 29, 30, 255));
            StyleColor borderColor = new StyleColor(new Color32(51, 51, 51, 255));

            miniMap.style.backgroundColor = backgroundColor;
            
            miniMap.style.borderTopColor = backgroundColor;
            miniMap.style.borderBottomColor = backgroundColor;
            miniMap.style.borderLeftColor = backgroundColor;
            miniMap.style.borderRightColor = backgroundColor;
            
        }

        private void AddSearchWindow()
        {
            if (searchWindow != null)
                return;

            searchWindow = ScriptableObject.CreateInstance<DSSearchWindow>();
            searchWindow.Initialize(this);

            nodeCreationRequest = context => SearchWindow.Open(new SearchWindowContext(context.screenMousePosition), searchWindow);

        }
        
        private void AddMiniMap()
        {
            miniMap = new MiniMap()
            {
                anchored = true
            };
            
            miniMap.SetPosition(new Rect(15,50, 200, 180));
            
            Add(miniMap);
            
            miniMap.visible = false;
        }

        #endregion

        #region Manipulators
        private void AddManipulators()
        {
            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            this.AddManipulator(CreateNodeContextualMenu("Add Node (Single Choice)", DialogueType.SingleChoice));
            this.AddManipulator(CreateNodeContextualMenu("Add Node (Multiple Choices)", DialogueType.MultipleChoice));
            this.AddManipulator(CreateGroupContextualMenu());
        }

        private IManipulator CreateNodeContextualMenu(string actionTitle, DialogueType dialogueType)
        {
            ContextualMenuManipulator contextualMenuManipulator = new ContextualMenuManipulator
                (
                    menuEvent => menuEvent.menu.AppendAction(actionTitle, actionEvent => AddElement(CreateNode("DialogueName",GetLocalMousePosition(actionEvent.eventInfo.localMousePosition), dialogueType)))
                );
            return contextualMenuManipulator;
        }

        private IManipulator CreateGroupContextualMenu()
        {
            ContextualMenuManipulator contextualMenuManipulator = new ContextualMenuManipulator
                (
                    menuEvent => menuEvent.menu.AppendAction("Add Group", actionEvent => CreateGroup("DialogueGroup", GetLocalMousePosition(actionEvent.eventInfo.localMousePosition)))
                );
            return contextualMenuManipulator;
        }
        #endregion

        #region Callbacks

        private void OnElementsDeleted()
        {
            deleteSelection=(operationName, askUser) =>
            {
                List<DSNode> nodesToDelete = new List<DSNode>();
                List<DSGroup> groupsToDelete = new List<DSGroup>();
                List<Edge> edgesToDelete = new List<Edge>();
                
                foreach (GraphElement element in selection)
                {
                    if (element is DSNode node)
                    {
                        nodesToDelete.Add(node);
                        if (string.IsNullOrEmpty(node.DialogueName))
                            RepeatedNamesAmount--;
                    }
                    else if (element is DSGroup group)
                    {
                        if (string.IsNullOrEmpty(group.title))
                            RepeatedNamesAmount--;
                        
                        groupsToDelete.Add(group);
                    }
                    else if (element is Edge edge)
                    {
                        edgesToDelete.Add(edge);
                    }
                }

                foreach (DSNode node in nodesToDelete)
                {
                    if (node.Group != null)
                        node.Group.RemoveElement(node);
                    
                    node.DisconnectAllPorts();
                    
                    RemoveUngroupedNode(node);
                    RemoveElement(node);
                }

                foreach (DSGroup group in groupsToDelete)
                {
                    List<DSNode> nodeList = new List<DSNode>();
                    
                    foreach (GraphElement element in group.containedElements)
                    {
                        if (element is DSNode node)
                        {
                            nodeList.Add(node);
                        }
                    }

                    group.RemoveElements(nodeList);
                    RemoveGroup(group);
                    RemoveElement(group);
                }
                
                DeleteElements(edgesToDelete);
            };
        }

        private void OnGroupElementsAdded()
        {
            elementsAddedToGroup = (group, elements) =>
            {
                foreach (GraphElement element in elements)
                {
                    if (element is DSNode)
                    {
                        DSNode node = (DSNode)element;
                        RemoveUngroupedNode(node);
                        AddGroupedNode(node, (DSGroup)group);
                    }
                }
            };
        }

        private void OnGroupElementsRemoved()
        {
            elementsRemovedFromGroup = (group, elements) =>
            {
                foreach (GraphElement element in elements)
                {
                    if (element is DSNode)
                    {
                        DSNode node = (DSNode)element;

                        RemoveGroupedNode(node, (DSGroup)group);
                        AddUngroupedNode(node);
                    }
                }
            };
        }

        private void OnGroupRenamed()
        {
            groupTitleChanged = (group, newTitle) =>
            {
                DSGroup dsGroup = (DSGroup)group;
                
                dsGroup.title = newTitle.RemoveWhitespaces().RemoveSpecialCharacters();
                
                if (string.IsNullOrEmpty(dsGroup.title))
                {
                    if (!string.IsNullOrEmpty(dsGroup.OldTitle))
                    {
                        RepeatedNamesAmount++;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(dsGroup.OldTitle))
                    {
                        RepeatedNamesAmount--;
                    }
                }
                
                RemoveGroup(dsGroup);
                dsGroup.OldTitle = dsGroup.title;
                
                AddGroup(dsGroup);
            };
        }

        private void OnGraphViewChanged()
        {
            graphViewChanged = (changes) =>
            {
                if (changes.edgesToCreate != null)
                {
                    foreach (Edge edge in changes.edgesToCreate)
                    {
                        DSNode node = (DSNode)edge.input.node;
                        DSChoiceSaveData choiceData = (DSChoiceSaveData)edge.output.userData;
                        choiceData.NodeID = node.ID;
                    }
                }

                if (changes.elementsToRemove != null)
                {
                    foreach (GraphElement element in changes.elementsToRemove)
                    {
                        if (element is Edge)
                        {
                            Edge edge = (Edge)element;
                            DSChoiceSaveData choiceData = (DSChoiceSaveData)edge.output.userData;
                            choiceData.NodeID = "";
                        }
                    }
                }

                return changes;
            };
        }

        #endregion
        
        #region Elements Creation
        public DSNode CreateNode(string nodeName, Vector2 position, DialogueType dialogueType, bool shouldDraw = true)
        {
            DSNode node;
            switch (dialogueType)
            {
                case DialogueType.SingleChoice:
                    node = new DSSingleChoiceNode();
                    break;

                case DialogueType.MultipleChoice:
                    node = new DSMultipleChoicesNode();
                    break;

                default:
                    node = new DSNode();
                    break;
            }
            
            node.Initialize(nodeName, this, position);

            if(shouldDraw)
                node.Draw();
            
            AddUngroupedNode(node);

            return node;
        }
        public DSGroup CreateGroup(String title, Vector2 mousePosition)
        {
            DSGroup group = new DSGroup(title, mousePosition);
            AddGroup(group);
            AddElement(group);

            foreach (GraphElement element in selection)
            {
                if (element is DSNode)
                {
                    DSNode node = (DSNode)element;
                    
                    group.AddElement(node);
                }
            }
            return group;
        }

        #endregion
        
        #region Repeated Elements

        public void AddUngroupedNode(DSNode node)
        {
            string nodeName = node.DialogueName.ToLower();

            if (!ungroupedNodes.ContainsKey(nodeName))
            {
                DSNodeErrorData nodeErrorData = new DSNodeErrorData();
                nodeErrorData.Nodes.Add(node);
                ungroupedNodes.Add(nodeName, nodeErrorData);
            }
            else
            {
                List<DSNode> ungroupedNodesList = ungroupedNodes[nodeName].Nodes;
                ungroupedNodesList.Add(node);
                Color errorColor = ungroupedNodes[nodeName].ErrorData.Color;
                node.SetErrorStyle(errorColor);

                if (ungroupedNodesList.Count == 2)
                {
                    RepeatedNamesAmount++;
                    ungroupedNodesList[0].SetErrorStyle(errorColor);
                }
            }
        }
        
        public void RemoveUngroupedNode(DSNode node)
        {
            string nodeName = node.DialogueName.ToLower();
            List<DSNode> ungroupedNodesList = ungroupedNodes[nodeName].Nodes;

            ungroupedNodes[nodeName].Nodes.Remove(node);
            node.ResetStyle();

            if (ungroupedNodesList.Count == 1)
            {
                RepeatedNamesAmount--;
                ungroupedNodesList[0].ResetStyle();
            }
            else if (ungroupedNodes[nodeName].Nodes.Count <= 0)
            {
                ungroupedNodes.Remove(nodeName);
            }
        }
        
        public void AddGroupedNode(DSNode node, DSGroup group)
        {
            string nodeName = node.DialogueName.ToLower();
            node.Group = group;
            
            if (!groupedNodes.ContainsKey(group))
            {
                groupedNodes.Add(group, new SerializableDictionary<string, DSNodeErrorData>());
            }
            
            if (!groupedNodes[group].ContainsKey(nodeName))
            {
                DSNodeErrorData nodeErrorData = new DSNodeErrorData();
                nodeErrorData.Nodes.Add(node);
                groupedNodes[group].Add(nodeName, nodeErrorData);
            }
            else
            {
                List<DSNode> groupedNodesList = groupedNodes[group][nodeName].Nodes;
                groupedNodesList.Add(node);
                Color errorColor = groupedNodes[group][nodeName].ErrorData.Color;

                node.SetErrorStyle(errorColor);

                if (groupedNodesList.Count == 2)
                {
                    RepeatedNamesAmount++;
                    groupedNodesList[0].SetErrorStyle(errorColor);
                }
            }
        }

        public void RemoveGroupedNode(DSNode node, DSGroup group)
        {
            string nodeName = node.DialogueName.ToLower();
            node.Group = null;
            
            if (!groupedNodes.ContainsKey(group) || !groupedNodes[group].ContainsKey(nodeName))
                return;
            
            List<DSNode> groupedNodesList = groupedNodes[group][nodeName].Nodes;

            groupedNodesList.Remove(node);
            node.ResetStyle();
            

            if (groupedNodesList.Count == 1)
            {
                RepeatedNamesAmount--;
                groupedNodesList[0].ResetStyle();
            }
            else if (groupedNodesList.Count == 0)
            {
                groupedNodes[group].Remove(nodeName);
                
                if (groupedNodes[group].Count == 0)
                    groupedNodes.Remove(group);
            }

        }

        public void AddGroup(DSGroup group)
        {
            string groupTitle = group.title.ToLower();

            if (!groups.ContainsKey(groupTitle))
            {
                DSGroupErrorData groupErrorData = new DSGroupErrorData();
                groupErrorData.Groups.Add(group);
                groups.Add(groupTitle, groupErrorData);
            }
            else
            {
                Color errorColor = groups[groupTitle].ErrorData.Color;
                group.SetErrorStyle(errorColor);
                
                List<DSGroup> groupsList = groups[groupTitle].Groups;
                groupsList.Add(group);
                
                if (groupsList.Count == 2)
                {
                    RepeatedNamesAmount++;
                    groupsList[0].SetErrorStyle(errorColor);
                }
            }
        }

        public void RemoveGroup(DSGroup group)
        {
            string oldGroupTitle = group.OldTitle.ToLower();
            List<DSGroup> groupList = groups[oldGroupTitle].Groups;

            groupList.Remove(group);
            group.ResetErrorStyle();
            
            if (groupList.Count == 1)
            {
                RepeatedNamesAmount--;
                groupList[0].ResetErrorStyle();
            }
            else if (groupList.Count == 0)
            {
                groups.Remove(oldGroupTitle);
            }
        }

        #endregion
        
        #region Overrided Methods
        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            List<Port> compatiblePorts = new List<Port>();
            ports.ForEach(port =>
            {
                if (startPort == port ||
                    startPort.node == port.node ||
                    startPort.direction == port.direction )
                {
                    return;
                }
                compatiblePorts.Add(port);
            });
            return compatiblePorts;
        }
        #endregion

        #region Utility Methods
        
        public Vector2 GetLocalMousePosition(Vector2 mousePosition, bool isSearchWindow = false)
        {
            Vector2 worldMouseLocation = mousePosition;
            if (isSearchWindow)
            {
                worldMouseLocation -= editorWindow.position.position;
            }

            return contentViewContainer.WorldToLocal(worldMouseLocation);
        }
        
        public void ClearGraph()
        {
            graphElements.ForEach(graphElement => RemoveElement(graphElement));
            
            groups.Clear();
            groupedNodes.Clear();
            ungroupedNodes.Clear();

            nameErrorsAmount = 0;
        }

        public void ToggleMiniMap()
        {
            miniMap.visible = !miniMap.visible;
        }

        #endregion
    }
}