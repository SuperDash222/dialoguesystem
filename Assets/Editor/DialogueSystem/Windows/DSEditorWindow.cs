using System.IO;
using UnityEditor;
using UnityEngine.UIElements;
using DialogueSystem.Utilities;
using UnityEditor.UIElements;


namespace DialogueSystem.Windows
{
    public class DSEditorWindow : EditorWindow
    {
        private DSGraphView graphView;
        private string defaultFileName = "DialogueFileName";
        private static TextField fileNameTextField;
        private Button saveButton;
        private Button miniMapButton;

        [MenuItem("Window/Dialogue System")]
        public static void Open()
        {
            GetWindow<DSEditorWindow>("Dialogue System");
        }

        private void OnEnable()
        {
            AddGraphView();
            AddStyles();
            AddToolBar();

        }
        #region Elements Addition
        private void AddGraphView()
        {
            graphView = new DSGraphView(this);

            rootVisualElement.Add(graphView);
            VisualElementExtensions.StretchToParentSize(graphView);
        }
        private void AddStyles()
        {
            rootVisualElement.AddStyleSheets("DialogueSystem/DSVariables.uss");
        }

        private void AddToolBar()
        {
            Toolbar toolbar = new Toolbar();

            fileNameTextField = DSElementUtility.CreateTextField(defaultFileName, "File Name:",
                callback =>
                {
                    fileNameTextField.value = callback.newValue.RemoveWhitespaces().RemoveSpecialCharacters();
                });
            saveButton = DSElementUtility.CreateButton("Save", () => Save());

            Button clearButton = DSElementUtility.CreateButton("Clear", () => Clear());
            Button resetButton = DSElementUtility.CreateButton("Reset", () => ResetGraph());
            Button loadButton = DSElementUtility.CreateButton("Load", () => Load());
            miniMapButton = DSElementUtility.CreateButton("MiniMap", () => ToggleMiniMap());

            toolbar.Add(fileNameTextField);
            toolbar.Add(saveButton);
            toolbar.Add(loadButton);
            toolbar.Add(clearButton);
            toolbar.Add(resetButton);
            toolbar.Add(miniMapButton);

            toolbar.AddStyleSheets("DialogueSystem/DSToolbarStyles.uss");

            rootVisualElement.Add(toolbar);
        }

        #endregion

        #region Toolbar Actions

        private void Save()
        {
            if (string.IsNullOrEmpty(fileNameTextField.value))
            {
                EditorUtility.DisplayDialog(
                    "Invalid File Name",
                    "Please ensure the file name you've typed in is valid.",
                    "Ok"
                    );
                return;
            }

            DSIOUtility.Initialize(graphView, fileNameTextField.value);
            DSIOUtility.Save();
        }
        
        private void Load()
        {
            string filePath = EditorUtility.OpenFilePanel("Dialogue Graph", "Assets/Editor/DialogueSystem/Graphs", "asset");

            if (!string.IsNullOrEmpty(filePath))
            {
                Clear();
                DSIOUtility.Initialize(graphView, Path.GetFileNameWithoutExtension(filePath));
                DSIOUtility.Load();
            }
        }

        private void Clear()
        {
            graphView.ClearGraph();
        }

        private void ResetGraph()
        {
            Clear();
            UpdateFileName(defaultFileName);
        }
        
        private void ToggleMiniMap()
        {
            graphView.ToggleMiniMap();
            miniMapButton.ToggleInClassList("ds-toolbar__button__selected");
        }
        
        #endregion
        
        #region Utility Methods

        public static void UpdateFileName(string newFileName)
        {
            fileNameTextField.value = newFileName;
        }

        public void EnableSaving()
        {
            saveButton.SetEnabled(true);
        }

        public void DisableSaving()
        {
            saveButton.SetEnabled(false);
        }

        #endregion
    }
}