using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace DialogueSystem.Inspectors
{
    using Utilities;
    using ScriptableObjects;
    
    [CustomEditor(typeof(DSDialogue))]
    public class DSInspector : Editor
    {
        //Dialogue Scriptable Objects
        private SerializedProperty dialogueContainerProperty;
        private SerializedProperty dialogueGroupProperty;
        private SerializedProperty dialogueProperty;


        // Filters
        private SerializedProperty groupedDialoguesProperty;
        private SerializedProperty startingDialoguesOnlyProperty;
        
        // Indexes
        private SerializedProperty selectedDialogueGroupIndexProperty;
        private SerializedProperty selectedDialogueIndexProperty;
        
        // Test
        private SerializedProperty textProperty;
        private SerializedProperty choice1Property;
        private SerializedProperty choice2Property;

        private void OnEnable()
        {
            dialogueContainerProperty = serializedObject.FindProperty("dialogueContainer");
            dialogueGroupProperty = serializedObject.FindProperty("dialogueGroup");
            dialogueProperty = serializedObject.FindProperty("dialogue");

            groupedDialoguesProperty = serializedObject.FindProperty("groupedDialogues");
            startingDialoguesOnlyProperty = serializedObject.FindProperty("startingDialoguesOnly");
            
            selectedDialogueIndexProperty = serializedObject.FindProperty("selectedDialogueIndex");
            selectedDialogueGroupIndexProperty = serializedObject.FindProperty("selectedDialogueGroupIndex");
            
            //Test
            textProperty = serializedObject.FindProperty("text");
            
            choice1Property = serializedObject.FindProperty("choice1");
            choice2Property = serializedObject.FindProperty("choice2");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
                
            textProperty.DrawPropertyField();
            
            choice1Property.DrawPropertyField();
            choice2Property.DrawPropertyField();
            
            DrawDialogueContainerArea();

            DSDialogueContainerSO dialogueContainer = (DSDialogueContainerSO) dialogueContainerProperty.objectReferenceValue;

            if (dialogueContainerProperty.objectReferenceValue == null)
            {
                StopDrawing("Select a Dialogue Container.");
                return;
            }

            DrawFiltersArea();

            bool currentStartingDialoguesOnlyFilter = startingDialoguesOnlyProperty.boolValue;
            
            List<string> dialogueNames;
            string dialogueFolderPath = $"Assets/DialogueSystem/Dialogues/{dialogueContainer.FileName}";
            string dialogueInfoMessage;
            

            if (groupedDialoguesProperty.boolValue)
            {
                List<string> dialogueGroupNames = dialogueContainer.GetDialogueGroupNames();

                if (dialogueGroupNames.Count == 0)
                {
                    StopDrawing("There are no Dialogue Groups in this Dialogue Container.");
                    return;
                }

                DrawDialogueGroupArea(dialogueContainer, dialogueGroupNames);
                
                DSDialogueGroupSO dialogueGroup = (DSDialogueGroupSO) dialogueGroupProperty.objectReferenceValue;
                dialogueNames = dialogueContainer.GetGroupedDialogueNames(dialogueGroup, currentStartingDialoguesOnlyFilter);

                dialogueFolderPath += $"/Groups/{dialogueGroup.GroupName}/Dialogues";
                dialogueInfoMessage = "There are no" + (currentStartingDialoguesOnlyFilter ? " Starting" : "") + " Dialogues in this Dialogue Group.";
            }
            else
            {
                dialogueNames = dialogueContainer.GetUngroupedDialogueNames(currentStartingDialoguesOnlyFilter);
                dialogueFolderPath += "/Global/Dialogues";
                
                dialogueInfoMessage = "There are no Ungrouped Dialogues in this Dialogue Container.";
            }

            if (dialogueNames.Count == 0)
            {
                StopDrawing(dialogueInfoMessage);
                return;
            }

            DrawDialogueArea(dialogueNames, dialogueFolderPath);
            serializedObject.ApplyModifiedProperties();
        }

        #region Draw Methods
        
        private void DrawDialogueContainerArea()
        {
            DSInspectorUtility.DrawHeader("Dialogue Container");
            dialogueContainerProperty.DrawPropertyField();
            
            EditorGUILayout.Space(4);
        }
        
        private void DrawFiltersArea()
        {
            DSInspectorUtility.DrawHeader("Filters");
            groupedDialoguesProperty.DrawPropertyField();
            startingDialoguesOnlyProperty.DrawPropertyField();
            
            EditorGUILayout.Space(4);
        }
        
        private void DrawDialogueGroupArea(DSDialogueContainerSO dialogueContainer, List<string> dialogueGroupNames)
        {
            DSInspectorUtility.DrawHeader("Dialogue Group");
            
            int oldSelectedDialogueGroupIndex = selectedDialogueGroupIndexProperty.intValue;

            DSDialogueGroupSO oldDialogueGroup = (DSDialogueGroupSO) dialogueGroupProperty.objectReferenceValue;
            
            bool isOldDialogueGroupNull = oldDialogueGroup == null;
            string oldDialogueGroupName = isOldDialogueGroupNull ? "" : oldDialogueGroup.GroupName;

            UpdateIndexOnNamesListUpdate(dialogueGroupNames, selectedDialogueGroupIndexProperty,
                                             oldSelectedDialogueGroupIndex, oldDialogueGroupName, isOldDialogueGroupNull);

            
            selectedDialogueGroupIndexProperty.intValue = DSInspectorUtility.DrawPopup("Dialogue Group", selectedDialogueGroupIndexProperty, dialogueGroupNames.ToArray());
            
            DSInspectorUtility.DrawDisabledFields(() => dialogueGroupProperty.DrawPropertyField());

            string selectedDialogueGroupName = dialogueGroupNames[selectedDialogueGroupIndexProperty.intValue];

            DSDialogueGroupSO selectedDialogueGroup = DSIOUtility.LoadAsset<DSDialogueGroupSO>($"Assets/DialogueSystem/Dialogues/{dialogueContainer.FileName}/Groups/{selectedDialogueGroupName}", selectedDialogueGroupName);
            
            dialogueGroupProperty.objectReferenceValue = selectedDialogueGroup;

            EditorGUILayout.Space(4);
        }
        
        private void DrawDialogueArea(List<string> dialogueNames, string dialogueFolderPath)
        {
            DSInspectorUtility.DrawHeader("Dialogue");

            int oldSelectedDialogueIndex = selectedDialogueIndexProperty.intValue;
            
            DSDialogueSO oldDialogue = (DSDialogueSO) dialogueProperty.objectReferenceValue;
            
            bool isOldDialogueNull = oldDialogue == null;
            string oldDialogueName = isOldDialogueNull ? "" : oldDialogue.DialogueName;

            UpdateIndexOnNamesListUpdate(dialogueNames, selectedDialogueIndexProperty, oldSelectedDialogueIndex, oldDialogueName, isOldDialogueNull);
            
            selectedDialogueIndexProperty.intValue = DSInspectorUtility.DrawPopup("Dialogue", selectedDialogueIndexProperty, dialogueNames.ToArray());

            string selectedDialogueName = dialogueNames[selectedDialogueIndexProperty.intValue];
            
            DSInspectorUtility.DrawDisabledFields(() => dialogueProperty.DrawPropertyField());

            DSDialogueSO selectedDialogue = DSIOUtility.LoadAsset<DSDialogueSO>(dialogueFolderPath, selectedDialogueName);

            dialogueProperty.objectReferenceValue = selectedDialogue;

            EditorGUILayout.Space(4);
        }

        private void StopDrawing(string message)
        {
            EditorGUILayout.HelpBox(message, MessageType.Info);
            EditorGUILayout.HelpBox("You need to select a Dialogue for this component to work properly at Runtime.", MessageType.Warning);
            serializedObject.ApplyModifiedProperties();
        }

        #endregion
        
        #region Index Methods
        
        private void UpdateIndexOnNamesListUpdate(List<string> optionNames, SerializedProperty indexProperty,
                                                      int oldSelectedPropertyIndex, string oldPropertyName, bool isOldPropertyNull)
        {
            if (isOldPropertyNull || indexProperty.intValue > optionNames.Count - 1)
            {
                indexProperty.intValue = 0;
                return;
            }
            if (oldSelectedPropertyIndex > optionNames.Count - 1 &&
                     oldPropertyName != optionNames[indexProperty.intValue])
            {
                if (optionNames.Contains(oldPropertyName))
                    indexProperty.intValue = optionNames.IndexOf(oldPropertyName);
                else
                    indexProperty.intValue = 0;
            }
        }
        
        #endregion
    }
}