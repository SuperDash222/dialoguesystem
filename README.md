# DialogueSystem

## About this project

This is a node-based dialogue system editor for Unity using the GraphView API.

## Features

- [x] Can create simple node and node with multiple choice.

- [x] Can create node group to keep de graph clean.

- [x] Can save/load a graph.

- [x] Added a minimap.


## Screenshots

### Editor
![Editor](Screenshots/DialogueSystemEditor.png)

### Example

#### **yes Answer:**
![Exemple](Screenshots/DialogueDemoYes.gif)

#### **No Answer:**
![Exemple](Screenshots/DialogueDemoNo.gif)


## Sources

Tutorial series on YouTube made by Indie Wafflus:
https://www.youtube.com/playlist?list=PL0yxB6cCkoWK38XT4stSztcLueJ_kTx5f

Unity Documentations:
https://docs.unity3d.com/ScriptReference/Experimental.GraphView.GraphView.html